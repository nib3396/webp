var Student = /** @class */ (function () {
    function Student(firstName, middleInitial, lastName) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    }
    return Student;
}());
function greeter(person) {
    return "Hello" + person.middleInitial + " " + person.lastName;
}
var user = new Student("Jane", "M.", "Meyer");
document.body.innerHTML = greeter(user);
