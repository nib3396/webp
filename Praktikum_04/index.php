<?php
	// Start Session for session Variables
	session_save_path('/opt/lampp/htdocs/');
	session_start();

	// Session ist notwendig, um die Daten wiederherzustellen nachdem "Abbrechen" gedrückt wurde
	if (isset($_GET['input_name']) && trim($_GET['input_name']) !== ''){
		$name = $_GET['input_name'];
		$_SESSION['session_name'] = $name;
	}else{
		if (isset($_SESSION['session_name'])){
			$name = $_SESSION['session_name'];
		}else{
			$name = 'Bart Simpson';
			$_SESSION['session_name'] = $name;
		}
	}

	if (isset($_GET['input_geburtsdatum']) && trim($_GET['input_geburtsdatum']) !== ''){
		$geburtsdatum = $_GET['input_geburtsdatum'];
		$_SESSION['session_geburtsdatum'] = $geburtsdatum;
	}else{
		if(isset($_SESSION['session_geburtsdatum'])){
			$geburtsdatum = $_SESSION['session_geburtsdatum'];
		}else{
			$geburtsdatum = 'xx.xx.1985';
			$_SESSION['session_geburtsdatum'] = $geburtsdatum; 
		}
	}

	if (isset($_GET['input_geburtsort']) && trim($_GET['input_geburtsort']) !== ''){
		$geburtsort = $_GET['input_geburtsort'];
		$_SESSION['session_geburtsort'] = $geburtsort;
	}else{
		if(isset($_SESSION['session_geburtsort'])){
			$geburtsort = $_SESSION['session_geburtsort'];
		}else{
			$geburtsort = 'Hollywood';
			$_SESSION['session_geburtsort'] = $geburtsort; 
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Willkommen auf der Homepage von Bart Simpson</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="./css/bootstrap.css">
	<link rel="stylesheet" href="./styles.css">
  <script src="./jquery.min.js"></script>
  <script src="./js/bootstrap.js"></script>
</head>
<body style=>

<div class="container">
  <h2>Willkommen auf der Homepage von Bart Simpson</h2>
  <div class="container container-for-tabs">
	  <ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab"  href="#home">Das bin ich</a></li>
		<li><a data-toggle="tab" href="#menu1" >Meine Vergangenheit</a></li>
		<li><a data-toggle="tab" href="#menu2">Was ich mag</a></li>
	  </ul>

	  <div class="tab-content">
		<div id="home" class="tab-pane fade in active">
		 <div class="tab-pane active" id="steckbrief">
                <h3 class="tab-heading">Mein Steckbrief:</h3>
                <div id="steckbriefSpacer" >&nbsp;</div>
                <div id="steckbriefDaten">
                    <!-- Hier Daten die erste Steckbrief Seite-->
                    <span id="steckbriefPicture">
						<img id="picture" class="steckbriefBild" alt="Fehler beim Laden des Bildes. Bitte gib einen g&uuml;ltigen Pfad ein!" src="./img/bart.jpeg">
					</span>
					<span class="span-data">
						<table class="table ">
							<tr>
								<td>Name</td>
								<td><?php echo $name?></td>
							</tr>
							<tr>
								<td>Geburtsdatum</td>
								<td><?php echo $geburtsdatum?></td>
							</tr>
							<tr>
								<td>Ort</td>
								<td><span><?php echo $geburtsort?></span></td>
							</tr>
							<tr>
								<td>. . .</td>
							</tr>
						</table>
					</span>
        </div>
      </div>
		</div>
		<div id="menu1" class="tab-pane fade">
		  <h3 class="tab-heading">Meine Vergangenheit</h3>
		  <p>...</p>
		</div>
		<div id="menu2" class="tab-pane fade">
		  <h3 class="tab-heading">Was ich mag</h3>
		  <p>...</p>
		</div>
	  </div>
  </div>
  
  <form method="get" action="./login.php">
    <button class="button-page" type="submit">Angaben ändern</button>
</form>
</div>

</body>
</html>
