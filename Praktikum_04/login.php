<?php
	if (isset($_GET['errorMessage'])){
		$errorMessage = $_GET['errorMessage'];
	}
	
?>

<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="./css/bootstrap.css">
	  <link rel="stylesheet" href="./styles.css">
  	<script src="./jquery.min.js"></script>
  	<script src="./js/bootstrap.js"></script>
</head>
<body>
</body>
<div class="container">
	<h2>Login zum Bearbeiten der Daten von Bart Simpson</h2>
	<form method="post" action = "checker.php">
		Passwort: 
		<input type="password" name="input_password">
		<button type="submit" > Login</button>
	</form>
	<div class="error-message">
		<?php
			if (isset($errorMessage)){
				echo $errorMessage;
			}
		?>
	</div>
	
</div>
</html>
