<?php
?>

<!DOCTYPE html>
<html>
<head>
	<title>Willkommen auf der Homepage von Bart Simpson</title>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="./css/bootstrap.css">
      <link rel="stylesheet" href="./styles.css">
  	<script src="./jquery.min.js"></script>
  	<script src="./js/bootstrap.js"></script>
</head>
<body>
</body>
<div class="container">
	<h2>Daten bearbeiten</h2>
	<form method="get" action="index.php">
		<div class="edit-form-div">
            <span class="tableSpan">Name</span>  <input type="text" name="input_name"> 
        </div>
        <div class="edit-form-div">
            <span class="tableSpan">Geburtsdatum</span> <input type="text" name="input_geburtsdatum"> 
        </div>
        <div class="edit-form-div">
            <span class="tableSpan">Geburtsort</span> <input type="text" name="input_geburtsort"> 
        </div>
        <div class="edit-form-div">
            <span class="loginButtons">
                <button type="submit" > Speichern</button>
                <a href="index.php" class="link_button">
                    <button type ="button">Abbrechen</button>
                </a>
            </span>
        </div>
	</form>
	
	<?php

		if (isset($errorMessage)){
			echo "Fehler".$errorMessage;
		}
	?>
</div>
</html>