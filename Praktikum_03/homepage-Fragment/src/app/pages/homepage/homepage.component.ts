import { Component, OnInit } from '@angular/core';
import { SpeicherService } from '../../services/SpeicherService.service';
import { Person } from './../../models/Person';

declare var jQuery: any;

@Component({
    selector: 'homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {
    public me: Person;
    public birthdateAsString: string;
    public steckBriefGesperrt: boolean;
    public lastBirthdate: string;
    public fehlermeldung: string = '';

    // Dialog-Attribute
    public username: string;
    public password: string;

    // Attribute zum Lesen von neuem Passwort und username
    public loginName: string;
    public loginPassword: string;

    public constructor(public speicherService: SpeicherService) {
        // Componenten-Daten initialisieren

        /* a) bitte Code hier einfügen... */
        this.me = this.speicherService.ladePerson();
        this.steckBriefGesperrt = true;
        this.lastBirthdate = this.me.birthdate;
        // Initialisieren von originalem Username und Passwort
        // Kann mit hinzugefügten HTML Elementen geändert werden
        this.loginName = 'hugo';
        this.loginPassword = '123';

    }

	// prueft die eingegebenen Daten (username/password) auf Korrektheit (Hugo/123)
	// und schliesst den Login-Dialog und ruft steckBriefAendern() auf, falls die Daten korrekt sind.
    // Andernfalls wird eine Fehlermeldung angezeigt.
    /** @?? Wieso soll nach dem Einloggen "steckBriefAendern()" aufgerufen werden
     * so wie das Programm funktioniert macht das wenig Sinn.
     * Ich kann es einbauen, aber dann muss ich eine Abfrage machen, woher "steckbriefAendern()" aufgerufen wird
     * und es bringt nicht mehr Funktionalität meiner Meinung nach.
     */
    public login(): void {

        /* b) bitte Code hier einfügen... */
        if (this.username === this.loginName && this.password === this.loginPassword){
            this.steckBriefGesperrt = false;
            this.username = '';
            this.password = '';
            this.cancelLogin();
            //this.steckBriefAendern();
        }else{
            this.fehlermeldung = 'Fehlerhafte Login-Daten';
        }

    };

	// der Login-Dialog wird geschlossen, die Fehlermeldung wird gelöscht.
    public cancelLogin(): void {
		/*
		 * Login-Dialog verbergen mit jQuery-Aufruf:
		 * es wird das Element mit id 'loginDialog' gesucht und 
		 * darauf die Methode 'modal' aufgerufen
		 */
        jQuery('#loginDialog').modal('hide');
        this.fehlermeldung = '';
    }

	// prüft zunächst das eingegebene 'birthdate'. Falls ein ungültiger oder leerer Wert vorliegt,
	// wird 'birthdate' aus dem letzten gültigen Wert wiederhergestellt.
	// Abhängig vom aktuellen Modus (nur lesen oder bearbeiten) werden nun entweder die geänderten Daten gespeichert
    // und der Bearbeitungsmodus wird verlassen oder es wird vom Lesemodus in den Bearbeitungsmodus gewechselt.
    /* @??  Der Modus ist doch egal. Das Programm funktioniert doch auch so wie gewünscht.
     * Das Datum kann ja nicht falsch sein, wenn vom Lesemodus in Bearbeiten gewechselt wird.
    */
    public steckBriefAendern(): void {

        /* c) bitte Code hier einfügen... */
        // Wenn leer wird das Alte wieder hergestellt
        // Ansonsten wird das alte backup wieder hergestellt
        if (this.me.birthdate === ''){
            this.me.birthdate = this.lastBirthdate;
        }else{
            this.lastBirthdate = this.me.birthdate;
        }

        this.steckBriefGesperrt = true;

    }

    public ngOnInit(): void {
    }
}
