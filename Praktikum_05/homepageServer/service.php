<?php

/**
 * auf true setzen, um DEBUG-Info in das PHP-ErrorLog zu schreiben
 */
$debugToErrorLog = true;

function __autoload($class_name) {
    include $class_name . '.php';
}


/*
 * Services
 */

/**
 * GET person: liefert eine Personenbeschreibung
 */
function getPerson() {
    // hier Ihr Code...
    $person = file_get_contents('./data/person.txt');
    echo $person;	// $person ist schon im JSON Format
}

/**
 * PUT person <JSON-Personenbeschreibung>: speichert eine Personenbeschreibung
 */
function putPerson($person) {
    // hier Ihr Code...
    file_put_contents('./data/person.txt', $person);
}

/**
 * POST login <Benutzername/Passwort>
 * Prüft anhand der in der Datei benutzer.txt abgelegten Daten, ob Benutzername und Passwort korrekt sind.
 * Setzt einen entsprechenden Statuscode, 200: OK, 404: Fehler
 */
function postLogin($data) {
    // hier Ihr Code...
    
    // Datei einlesen und die Schlüssel-Wert Paare extrahieren
    $correctData = file_get_contents('./data/benutzer.txt');
													// Zeile 1: benutzername=hugo
    $lines = explode( PHP_EOL, $correctData);		// Zeile 2: passwort=123
    // $lines ist ein Feld mit den Zeilen als Einträgen
    // Nochmals jede Zeile beim "=" trennen
   
    $user = explode('=', $lines[0]);	// $usr: Feld mit [0] benutzer, [1] hugo
    $pwd = explode('=', $lines[1]);		// $usr: Feld mit [0] passwort, [1] 123
    
    // Argument abrufen und dekodieren
    // "True wird benötigt, um ein Feldzu erzeugen, ohne Parameter ist es ein Objekt"
    $loginData = json_decode($data, true);	

	// Wenn Nurzername und PAsswort übereinstimmen, OK Status zurückgeben
	// Nutzername muss getrimmt werden, da das Newline Symbol nicht gelöscht wird beim explode
    if ($loginData ["benutzername"] === trim($user[1]) && $loginData ["passwort"] === $pwd [1]){
		http_response_code(200); 
	}else{
		http_response_code(404); 
	}
    
    
}

/*
 * Service Dispatcher
 */

$url = $_REQUEST['_url'];
$requestType = $_SERVER['REQUEST_METHOD'];
$body = file_get_contents('php://input');
$jsonData = json_decode($body);

if ($GLOBALS["debugToErrorLog"]) {
    error_log("REST-Call: ".$requestType.' '.$url.':'.$body);
}

if ($url === '/person') {
    // hier ihr Code...
    if ($requestType === 'GET') {
         getPerson();
     }else if ($requestType === 'PUT'){
		 putPerson($body);
	 }
} else if ($url === '/login'){
	if ($requestType === 'POST'){
		postLogin($body);
	}
} else {
    badRequest($requestType, $url, $body);
}


function badRequest($requestType, $url, $body) {
 	http_response_code(400);
    if ($GLOBALS["debugToErrorLog"]) {
        error_log("bad request");
    }
    die('Ungültiger Request: '.$requestType.' '.$url.' '.$body);
}

?>
