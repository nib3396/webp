/*
 * Data Transfer Object für Fachobjekt Adresse
 */
class AdresseDTO  {
	
	constructor(id, name, email, ort, plz, strasse) {
		this._id = id;
		this._name = name;
		this._email = email;
		this._ort = ort;
		this._plz = plz;
		this._strasse = strasse;
	}

	get id() {
		return this._id;
	}
	get name() {
		return this._name;
	}
	get email() {
		return this._email;
	}
	get ort() {
		return this._ort;
	}
	get plz() {
		return this._plz;
	}
	get strasse() {
		return this._strasse;
	}
	set id(wert) {
		this._id = wert;
	}
	set name(wert) {
		this._name = wert;
	}
	set email(wert) {
		this._email = wert;
	}
	set ort(wert) {
		this._ort = wert;
	}
	set plz(wert) {
		this._plz = wert;
	}
	set strasse(wert) {
		this._strasse = wert;
	}


	// Name muss sein "Vorname, Nachname"
	validateName(name){
		var re = /^[a-zA-Z]+, [a-zA-Z]+$/;			
		
		return re.test(name);
	}

	/**
	 * Prüft die Attribute einzeln auf Korrektheit.
	 * Bei einem Fehler wird eine Exception (=Fehlermeldung als String) geworfen.
	 * Beim Aufrufer sollte diese gefangen werden und z.B. an der Benutzeroberfläche als Fehler-
	 * meldung ausgegeben werden.
	 */
	pruefe() {
		// *** (1) ***

		if (this.validateID (this._id) === false){
			throw "ID ist falsch";
		}

		if (this.validateName(this._name) === false){
			throw "Name ist falsch";
		}

		if (this.validateEmail(this._email) === false){
			throw "E-Mail ist falsch";
		}

		if (this.validateOrt(this._ort) === false){
			throw "Ort ist falsch";
		}

		if (this.validatePLZ(this._plz) === false){
			throw "Postleitzahl ist falsch";
		}

		if (this.validateStrasse(this._strasse) === false){
			throw "Straße ist falsch";
		}
		
    }

	validateID (id){
		var re = /^-?[0-9]+$/;
		return re.test(id);
	}

	

	/**
	 * Liefert true, falls 'email' eine korrekte E-Mail-Adresse enthält.
	 */
	validateEmail(email) {
    	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    	return re.test(email);
	}

	// Ort Muss aus Buchstaben bestehen
	validateOrt (ort){
		var re = /^[a-zA-Z]+$/;
		return re.test(ort);
	}

	validatePLZ (plz){
		var re = /^[0-9]+$/;
		return re.test(plz);
	}

	// Strasse muss sein "Straßenname Hausnummer" Hausnummer muss Nummer sein
	validateStrasse (strasse){
		var re = /^[a-zA-Z ]+ [0-9]+$/;
		return re.test(strasse);
	}

    toString() {
    	return "{" + this.id + ", " +
    		this.name + ", " + 
    		this.email + ", " + 
    		this.ort + ", " + 
    		this.plz + ", " + 
    		this.strasse + "}";
    }
}
