"use strict";

var adressenDAO = AdressenDAO.gibAdresseDAO();


// Lädt die Tabelle neu, wenn das Fenster den Focus bekommt.
// Notwendig zum Aktualisieren nach dem Ändern/Anlegen einer Adresse
window.onfocus = function (evt) {
	var table = document.getElementById("adressenTabelle");
	
	console.log("table = " + table);
	if (table == null) { // passiert nur, wenn wir in anderem Tab sind...
		return;
	}
	
	if (evt.type == 'focus') {		
		console.log('focus');
		belegeAdressenTabelle();
	}			
};

/*
 * Handler für Suchdialog
 */
function neueAdresseAnlegen() { 
	window.open("NeueAdresse.html?mode=new");			// Öffnet NeueAdrese.html im "edit" Modus
}


function bearbeiteAdresse(button) { 
	var url = "NeueAdresse.html?mode=edit";					// URL Ort und Mode setzen
	
	try {
		var row = button.parentNode.parentNode;				// Zeile in der Tebelle mit dem zu bearbeitenden Eintrag
		
		url += "&id=" + row.cells[0].innerHTML;				// Werte auslesen und an den URL anhängen
		url += "&name=" + row.cells[1].innerHTML;			// "&" als Trenner der Key-Value Pare
		url += "&email=" + row.cells[2].innerHTML;			// Wird später mit "split("&")" zerlegt
		url += "&ort=" + row.cells[3].innerHTML;
		url += "&plz=" + row.cells[4].innerHTML;
		url += "&strasse=" + row.cells[5].innerHTML;
	    window.open(encodeURI(url));						// Öffnet neues Fenster und lädt die Edit Seite
 	} catch (error) {
 		alert(error);
 	}
}

// Ruft Löschen Funktion der Adresse auf.
// Setzt ID auf -1 und löscht die Zeile aus der Tabelle
function loescheAdresse(buttonWidget) {
    var index = buttonWidget.parentNode.parentNode.rowIndex;				// Zeilennummer in der Tabelle
	var id = Number(buttonWidget.parentNode.parentNode.cells[0].innerHTML);	// ID des Elements

    adressenDAO.loescheAdresse(id);											// ID des Eintrags auf "-1" setzen
    document.getElementById('adressenTabelle').deleteRow(index);			// Zeile löschen
}

/*
 * Hilfsfunktionen für Suchdialog
 */
function belegeZeile(table, adresse) {
	console.log("belegeZeile: adresse = " + adresse.toString());

	var tr = table.insertRow(1); // Überschrift überspringen	
	var td  = tr.insertCell(0);
	
   	var inhalt  = document.createTextNode(adresse.id);	// Spalte mit ID wird versteckt
   	td.appendChild(inhalt);
   	td.hidden = "true";
   	
	td  = tr.insertCell(1);
   	inhalt = document.createTextNode(adresse.name);
   	td.appendChild(inhalt);

	// *** (5) ***

	td  = tr.insertCell(2);
	inhalt = document.createTextNode(adresse.email);
	td.appendChild(inhalt);

	td  = tr.insertCell(3);
	inhalt = document.createTextNode(adresse.ort);
	td.appendChild(inhalt);

	td  = tr.insertCell(4);
	inhalt = document.createTextNode(adresse.plz);
	td.appendChild(inhalt);

	td  = tr.insertCell(5);
	inhalt = document.createTextNode(adresse.strasse);
	td.appendChild(inhalt);

	td  = tr.insertCell(6);									// Letzte Zelle in der Tabelle
	// edit button
	var button = document.createElement('button');			// Erstellt "Edit" button
	button.onclick = function() {
		bearbeiteAdresse(this);								// Ruft beim Klicken die "bearbeite Adresse" Funktion auf
	};														// Gibt sich selbst (Den Button) als Argument mit
	var image = document.createElement('img');				// Erstellt das Image und setzt die Eigenschaften
	image.src = "images/editIcon.jpg";
	image.width = "15";
	image.height = "15";
	button.appendChild(image);								// Fügt das Bild an den Button an
	
	td.appendChild(button);									// Fügt den Button der Tabelle hinzu
	// delete button

	// *** (6) ***
	button = document.createElement('button');
	button.onclick = function () {
		loescheAdresse(this);
	};
	image = document.createElement('img');
	image.src = "images/trashIcon.jpg";
	image.width = "15";
	image.height = "15";
	button.appendChild(image);
	td.appendChild(button);
}

function belegeAdressenTabelle() {
	try {
		console.log("belegeAdressenTabelle");						// Ausgabe auf der Konsole
	
		var table = document.getElementById("adressenTabelle");		// table enthält die Tabelle mit den Adressen
		var adressen = adressenDAO.findeZuFilterUndSortiere(		// adressen ist ein Feld mit den anzuzeigenden, soriterten Adresen
			document.getElementById('nameID').value,				// Liest das Feld "Name"
			document.getElementById('ortID').value,					// Liest Inhalt des Feldes "Ort"
			document.getElementById('sortierungID').value);			// Gibt zurück, was sortert werden soll
		var i;
		
		// Löscht alle Zeilen der Tabelle bis auf die Kopfzeile
		while (table.rows.length > 1) {
			table.deleteRow(1);
		}

		for (i = 0; i < adressen.length; ++i) {			// Für alle anzuzeigenden Adressen
			belegeZeile(table, adressen[i]);			// Erstellt die Zeile in der Tabelle
		}
	} catch (error) {
		console.log(error);
		alert(error);
	}
}

/*
 * Handler für Dialog zum Anlegen/Bearbeiten von Adressen
 */
function adresseBearbeitenAbbrechen() {
	window.close();
}

// Vom Such Dialog
// Wird vom "Speichern" Button aufgerufen
function speichereAdresse() {
	
	var id = document.getElementById("idID").value;			// Sehen, welche ID im versteckten Eingabefeld ist
	var adresse = new AdresseDTO(id,						// Neues Adress Objekt mit Werten aus dem 
			document.getElementById("nameID").value,
			document.getElementById("emailID").value,
			document.getElementById("ortID").value,
			document.getElementById("plzID").value,
			document.getElementById("strasseID").value);
			
    try {
    	adresse.pruefe();									// Prüft auf gültige Adresse
    	if (id == -1) {										// Wenn eine neue Angelegt wird
    		adressenDAO.neueAdresse(adresse);    			// Neue anlegen. Adresse bekommt ihre ID zugewiesen
    	} else {											// Ansonsten
    		adressenDAO.aktualisiereAdresse(adresse);    	// Bestehende überschreiben, Adresse hat bereits eine ID und behält sie
    	}    	
    	window.close();
    } catch (errorMsg) {
        alert(errorMsg);
    }
}

/*
 * Hilfsfunktionen für Dialog zum Bearbeiten/Neuanlegen
 */
function initialisiereSeite() {
	var url = decodeURI(window.location.href);
    var urlParts = url.split("?");							// urlParts ist ein Feld. Zeichenketten nach "?" getrennt
    var modeParts = urlParts[1].split("&");					// Teil nach "?" wird nun zerlegt in Einzelteile mit "&" als Trenner
    var newText = "Neue Adresse anlegen";							// modeParts enthält Textfeld Einträge "key=value"
    var bearbeitenText = "Adresse bearbeiten";
    
    if (modeParts[0] == "mode=new") {											// Wenn neue Adresse angelegt wird
        document.getElementById("titleID").innerHTML = newText;					// Felder leer lassen
        document.getElementById("ueberschriftID").innerHTML = newText;
        document.getElementById("idID").value="-1";								// ID mit -1 Initialisieren
    } else {																	// Wenn Beartbeiten
        document.getElementById("titleID").innerHTML = bearbeitenText;
        document.getElementById("ueberschriftID").innerHTML = bearbeitenText;
        document.getElementById("idID").value = modeParts[1].split("=")[1];		// Alle Key-Value Paare werden beim "=" getrennt
        document.getElementById("nameID").readonly = true;						// Name darf nicht geändert werden
        document.getElementById("nameID").disabled = true;
        document.getElementById("nameID").value = modeParts[2].split("=")[1];
        document.getElementById("emailID").value = modeParts[3].split("=")[1];
        document.getElementById("ortID").value = modeParts[4].split("=")[1];
        document.getElementById("plzID").value = modeParts[5].split("=")[1];
        document.getElementById("strasseID").value = modeParts[6].split("=")[1];
    }
}

