/*
 * Data Access Object (Datenzugriff) für adresseen-Fachobjekte  ------
 */

class AdressenDAO {
	constructor() {
		this._adressenArray = [];	// hält AdtesseDTO Objekte bestehend aus id, name, ...
	}

	/* 
	 * laden und speichern in local storage
	 */
	speichern() {
		localStorage.setItem('adressenDAO', JSON.stringify(this));
	}

	// Füllt das Atribut AdressenArray[] mit den Adressen
	laden() {
		var i;
		var gespeichertesDAOalsString = localStorage.getItem('adressenDAO');	// Lädt JSOM String
		var gespeichertesDAO = JSON.parse(gespeichertesDAOalsString);			// Parst den String
		
		// Ersetzt das aktuelle Feld mit Adressen mit dem aus dem lokalen Speicher
		if (gespeichertesDAO != null)
			this._adressenArray = gespeichertesDAO._adressenArray;
		
		// Füllt das Feld mit AdresseDTO Objekten
		// Ersetzt für jeden Index das JSON Objekt mit AdresenDTO
		for (i = 0; i < this._adressenArray.length; ++i) {
			this._adressenArray[i] = new AdresseDTO(
				this._adressenArray[i]._id,
				this._adressenArray[i]._name,
				this._adressenArray[i]._email,
				this._adressenArray[i]._ort,
				this._adressenArray[i]._plz,
				this._adressenArray[i]._strasse);
		}
		
		console.log('gespeichertesDAO: ', gespeichertesDAO);
	}

	/*
	 * Hilfsfunktionen
	 */

	hatPrefix(str, prefix) {
		return str.indexOf(prefix) === 0;
	}

	/**
	 * Liefert true, wenn die übergebene Adresse dem Namensfilter und/oder dem Adressfilter entspricht.
	 * Beide Filter sind Präfixfilter, d.h. sie sind dann true, wenn 'name' oder 'ort' ein Präfix des
	 * entsprechenden Attributs der Adresse ist. Leere Filterwerte werden dabei ignoriert, d.h. der Filter 
	 * wird nicht geprüft.
	 * Beispiele: 
	 * 		name='P' und ort='Ing', adresse.name='Peter' und adresse.ort='Ingolstadt': Ergebnis = true
	 * 		('P'' ist Präfix von 'Peter', 'Ing'' ist Präfix von 'Ingolstadt'') 
	 * 		name='' und ort='I', adresse.name='Peter' und adresse.ort='ngolstadt': Ergebnis = false 
	 * 		(der Namensfilter wird nicht evaluiert, 'I'' ist nicht Präfix von 'ngolstadt'')
	 */
	filter(adresse, name, ort) {
		// *** (2) ***
		
		// Rückbage mit false initialisiert
		let ret = false;
		
		// Rückgabe wird wahr, wenn beide Felder das richtige Prefix haben
		// Leere Felder haben immer das richtige Prefix
		if (this.hatPrefix(adresse.name, name) && this.hatPrefix(adresse.ort, ort)){
			ret = true;
		}

		return ret; 			// Rückgabe von true, wenn ein Prefix rightig erfüllt ist
	};	
	
	/**
	 * Gibt das übergebene AdresseDTO-Array 'liste'' sortiert nach 'sortierung' (= string-Wert 
	 * Name, Ort oder PLZ) zurück. Abhängig vom Wert von 'sortierung' wird eine passende sortierFunktion
	 * definiert, die dann für die Sortierung mit "sort" genutzt wird.
	 */
	sortiereAdressenListe(liste, sortierung) {
		// *** (3) ***

		// Sortiert verkeht herum, da die Zeilen an die Tabelle der Reihe nach angefängt werden
		// Also erstes Listenelement ist am Ende ganz unten in der Tabelle
		switch (sortierung){
			case "Name":
				liste.sort(function(a,b) {
					if (a.name < b.name){
						return 1;
					}
					if (a.name > b.name){
						return -1;
					}
					return 0;
				});
				break;
			case "Ort":
				liste.sort(function(a,b) {
					if (a.ort < b.ort){
						return 1;
					}
					if (a.ort > b.ort){
						return -1;
					}
					return 0;
				});
				break;
			case "PLZ":
				liste.sort(function(a,b) {
					if (a.plz < b.plz){
						return 1;
					}
					if (a.plz > b.plz){
						return -1;
					}
					return 0;
				});
				break;
		}

		return liste;
	}

	/*
	 * Methoden zum Zugriff auf die adresseenliste
	 */
	findeAdresseZuId(id) {
		this.laden();						// Füllt das Atribut AdressenArray[] mit den Adressen
		var p = this._adressenArray[id];	// Lädt die gewünschte Adresse
		
		return p;							// Gibt das Adressen Objekt zurück
	}
	
	// Gibt Feld mit allen nicht gelösht markierten DTO Objekten (Adress Einträgen zurück)
	// Feld ist nach id sotrtiert
	findeAlle() {
		this.laden();											// Lokal Storage lesen und Feld mit DTO Objekten füllen
		var ergebnis = [];										// Feld, das zurückgegeben wird
		var i, j = 0;
		
		// Füllt ergebnis[]
		for (i = 0; i < this._adressenArray.length; ++i) {		// Für alle Adressen
			if (this._adressenArray[i].id != -1) {				// Wenn nicht als gelöscht markiert
				ergebnis[j++] = this._adressenArray[i];			// In das Ergebnis Feld speichern + Ergebnis Zähler eins weiter setzen
			}													
		}
		
		// sortiert ergebnis[]
		ergebnis.sort(
			function(p1, p2) { 
				return p1.id - p2.id;
			}
		);

		return ergebnis;
	}

	// Filtert die Adressen nach dem ausgewählten Kriterium
	// Sortiert dia ausgewählten nach dem eingestellten Filter
	findeZuFilterUndSortiere(name, ort, sortierung) {
		this.laden();					// Füllt das Atribut AdressenArray[] mit den Adressen		
		var ergebnis = [];				// Enthält alle gefilterten Elemente
		var i, j = 0;
		
		// Sucht alle, dem Filter entsprechenden Adressen heraus und speichert sie in ergebnis[]
		for (i = 0; i < this._adressenArray.length; ++i) {				// Für alles ADressen
			if (this._adressenArray[i].id != -1) {						// Wenn Adresse nicht als gelöscht markeirt
				if (this.filter(this._adressenArray[i], name, ort)) { 	// Prüft, ob der Eintrag dem Filterkriterium entspricht
					ergebnis[j++] = this._adressenArray[i];				// Falls ja, wird er ergebnis[] hinzugefügt
				}
			}
		}
		
		// Sortiert das Ergebnis nach dem gewünschtem Kriterium
		this.sortiereAdressenListe(ergebnis, sortierung);
		// Rückgabe aller des sortierten Feldes
		return ergebnis;
	}

	// Fügt eine Adresse hinzu
	// Ersetzt die erste als gelöscht markierte, oder geht ans Ende des Feldes, falls keine "ID=-1" hat
	neueAdresse(adresse) {
		this.laden();				// Füllt das Atribut AdressenArray[] mit den Adressen
		var i;
		
		// Geht zur ersten Adresse, die als gelöscht markiert ist
		// Oder ans Ende des Feldes, wenn keine gelöscht ist
		for (i = 0; i < this._adressenArray.length; ++i) {
			if (this._adressenArray[i].id == -1) {
				break;
			}
		}
		// Setzt die ID wirder zu einem micht gelöschten Index (id = Position im Feld)
		adresse.id = i;
		this._adressenArray[adresse.id] = adresse;		// Überschreibt die als gelöscht markierte Adresse mit der neuen
		this.speichern();								// Die neue Adressen Liste wird in den LocalStorage gespeichert
	}

	// Ersetzt eine Adresse im Feld
	// Die Adresse mit der gleichen ID (id = Index im Feld) wird mit der neuen ersetzt
	aktualisiereAdresse(adresse) {
		this.laden();												// Füllt das Atribut AdressenArray[] mit den Adressen
		if (this.findeAdresseZuId(adresse.id) !== "undefined") {	// Wenn die zu aktualisierende Adresse eine gültigt ID besitzt
			this._adressenArray[adresse.id] = adresse;				// Adresse im Feld mit der neuen überschreiben
			this.speichern();										// Neues Feld in den Local Storage schreiben
		}
	};

	/**
	 * Löscht das Adressobjekt mit der übergebenen ID.
	 * Es wird nur "logisch" gelöscht, indem die id auf den Wert -1 gesetzt wird.
	 */
	loescheAdresse(id) {
		// *** (4) ***
		this._adressenArray[id].id = -1;				// ID vom zu löschenden Objekt auf "-1" setzen
		//aktualisiereAdresse(this._adressenArray[id]);	// Das als gelöscht markierte Objekt aktualisiern
		this.speichern();
	}

	/*
	* Getter für adresseDAO ---------------------------------------------
	*/
	// Gibt alle Adressen im LocalStorage auf der Konsole aus
	static gibAdresseDAO() {
		var dao = "undefined";
		
		if (typeof(Storage) !== "undefined") {
			dao = new AdressenDAO();		
			if (localStorage['adressenDAO']) {								// Wenn sdressenDAO im Lokalen Spricher
				try {
					var i;
					
					dao.laden();											// Dao laden
					for (i = 0; i < dao._adressenArray.length; ++i) {		// Für jede Adresse
						var p = dao._adressenArray[i]; 						// Die Adresse
						console.log(p.toString());							// An der Konsole ausgeben
					}

				} catch (error) {
					alert(error);
				}
			}
		} else {
			alert("Sorry, your browser does not support web storage…");
		}	
		
		return dao;
	}
}
